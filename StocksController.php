<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Stock;
use App\Models\Product;
use Validator;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class StocksController extends Controller
{
    private $rules = [
        'name' => 'required',
        'url_alias' => 'required|unique:stocks',
        'start_time' => 'required',
        'end_time' => 'required',
        'meta_title' => 'required|max:75',
        'meta_description' => 'max:180',
        'meta_keywords' => 'max:180',
    ];

    private $messages = [
        'name.required' => 'Поле должно быть заполнено!',
        'start_time.required' => 'Поле должно быть заполнено!',
        'end_time.required' => 'Поле должно быть заполнено!',
        'meta_title.required' => 'Поле должно быть заполнено!',
        'meta_title.max' => 'Максимальная длина заголовка не может превышать 75 символов!',
        'meta_description.max' => 'Максимальная длина описания не может превышать 180 символов!',
        'meta_keywords.max' => 'Максимальная длина ключевых слов не может превышать 180 символов!',
        'url_alias.required' => 'Поле должно быть заполнено!',
        'url_alias.unique' => 'Значение должно быть уникальным для каждой акции!'
    ];

    /**
     * admin methods
     */

    public function index()
    {
        $stocks = Stock::paginate(15);

        return view('admin.stocks.index', ['stocks' => $stocks]);
    }

    public function create()
    {
        return view('admin.stocks.create');
    }

    public function store(Request $request, Stock $stock)
    {
        $validator = Validator::make($request->all(), $this->rules, $this->messages);


        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                ->withErrors($validator);
        }
        $request->merge(['discount' => 0]);
        $stock->fill($request->except('_token'));
        $stock->save();
        $stock->products()->attach($request->settings['products']);
        Cache::flush();

        return redirect('/admin/stocks')
            ->with('message-success', 'Акция ' . $stock->name . ' успешно добавлена.');
    }

    public function edit($id)
    {
        return view('admin.stocks.edit')
            ->with('stock', Stock::find($id));
    }

    public function update($id, Request $request, Stock $stock)
    {
        $rules = $this->rules;
        $rules['url_alias'] = 'required|unique:stocks,url_alias,'.$id;
        $validator = Validator::make($request->all(), $rules, $this->messages);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                ->withErrors($validator);
        }

        $stock = $stock->find($id);
        $stock->update($request->except('_token'));
        $stock->save();
        $stock->products()->detach();
        $stock->products()->attach($request->settings['products']);
        Cache::flush();

        return redirect('/admin/stocks')
            ->with('message-success', 'Акция ' . $stock->name . ' успешно добавлена.');
    }
    public function destroy($id)
    {
        $stock = Stock::find($id);
        $stock->delete();
        Cache::flush();

        return redirect('/admin/stocks')
            ->with('message-success', 'Акция ' . $stock->name . ' успешно удалена.');
    }


    /**
     * public methods
     */
    public function show()
    {
        $stock = new Stock();
        $today_now = Carbon::now();
        $to_view = $stock->where('end_time', '>', $today_now)
            ->where('start_time', '<', $today_now)->paginate(4);
        return view('public.stocks')
            ->with('stocks', $to_view);
    }
    public function showOne($alias)
    {
        return view('public.stock')
            ->with('stock', Stock::where('url_alias', $alias)->first());
    }
}
