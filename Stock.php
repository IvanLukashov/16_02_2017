<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Stock extends Model
{
    public $table = 'stocks';
    use SoftDeletes;
    protected $fillable = [
        'name',
        'description',
        'url_alias',
        'start_time',
        'end_time',
        'discount',
        'status',
        'image_id',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];
    public function image()
    {
        return $this->hasOne('App\Models\Image','id', 'image_id');
    }
    public function products()
    {
        return $this->belongsToMany('App\Models\Product','products_stock', 'stock_id', 'product_id');
    }

    public function now_stocks()
    {
        $today_now = Carbon::now();
        return $this->where('end_time', '>', $today_now)
            ->where('start_time', '<', $today_now)->get();
    }
    public function start_date()
    {
        return date('d.m.Y', strtotime($this->start_time));
    }
    public function end_date()
    {
        return date('d.m.Y', strtotime($this->end_time));
    }
}
